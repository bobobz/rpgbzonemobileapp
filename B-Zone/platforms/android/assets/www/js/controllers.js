angular.module('bzoneMobileApp.controllers', [])

    .controller('driversController', function ($scope, ergastAPIservice) {
        $scope.nameFilter = null;
        $scope.driversList = [];
        $scope.searchFilter = function (driver) {
            var re = new RegExp($scope.nameFilter, 'i');
            return !$scope.nameFilter || re.test(driver.Driver.givenName) || re.test(driver.Driver.familyName);
        };

        ergastAPIservice.getDrivers().success(function (response) {
            //Digging into the response to get the relevant data
            $scope.driversList = response.MRData.StandingsTable.StandingsLists[0].DriverStandings;
        });
    })

    .controller('driverController', function ($scope, $routeParams, ergastAPIservice) {
        $scope.id = $routeParams.id;
        $scope.races = [];
        $scope.driver = null;

        ergastAPIservice.getDriverDetails($scope.id).success(function (response) {
            $scope.driver = response.MRData.StandingsTable.StandingsLists[0].DriverStandings[0];
        });

        ergastAPIservice.getDriverRaces($scope.id).success(function (response) {
            $scope.races = response.MRData.RaceTable.Races;
        });
    })

    .controller('accountController', function ($scope, bzoneAPIservice) {
        bzoneAPIservice.getMyAccountInfo();
        $scope.account = bzoneAPIservice.getSelectedAccount();
    })

    .controller('accountsController', function ($scope, $location, bzoneAPIservice, localStorageService) {
        //bzoneAPIservice.updateAccountsList();
        $scope.accounts = bzoneAPIservice.getAccountsList();
        $scope.selectedAccount = bzoneAPIservice.getSelectedAccount();
        $scope.removeAccount = function (account) {
            bzoneAPIservice.removeAccount(account)
                .then(function (res) {
                    if (res !== false) {
                        $scope.selectedAccount = res;
                    }
                })
        };
        $scope.switchAccount = function (account) {
            bzoneAPIservice.switchAccount(account);
            $scope.selectedAccount = bzoneAPIservice.getSelectedAccount();
        };
    })

    .controller('loginController', function ($scope, $location, bzoneAPIservice) {
        $scope.logged = "false";
        $scope.username = $scope.password = $scope.server = null;
        $scope.selectedAccount = [];
        $scope.submit = function () {
            if ($scope.username) {
                bzoneAPIservice.checkLoginData($scope.username, $scope.password, $scope.server)
                    .then(function (res) {
                        if (res === "false") {
                            $scope.logged = "Wrong password or invalid nickname!";
                        }
                        else if (res === "linkedaccount") {
                            $scope.logged = "This account is already linked to a mobile device!";
                        }
                        else if (res === "true") {
                            $scope.logged = "Succes! Logged in!";
                            $scope.selectedAccount = bzoneAPIservice.getSelectedAccount();
                            bzoneAPIservice.updateOneSignalUserID();
                        }
                        else {
                            $scope.logged = "Error! Couldn't connect to the RPG Server!";
                        }
                    }, function (res) {
                        $scope.logged = "Error! Couldn't connect to the RPG Server!";
                    });
            }
        };
    })

    .controller('homeController', function ($scope, $location, bzoneAPIservice) {
    })

    .controller('headerController', function ($scope, $location, bzoneAPIservice) {
        $scope.getName = function () {
            return bzoneAPIservice.getHeaderAccountName();
        };

        $scope.getServer = function () {
            return bzoneAPIservice.getHeaderServer();
        };

        $scope.updateAccountStatus = function () {
            $scope.selectedAccount = bzoneAPIservice.getSelectedAccount();
            bzoneAPIservice.checkOnlineStatus($scope.selectedAccount);
        };

        var updateStatusTimer = setInterval($scope.updateAccountStatus, 15000);

        $scope.getStatus = function () {
            return bzoneAPIservice.getHeaderAccountStatus();
        };

        $scope.updateAccountsList = function () {
            bzoneAPIservice.updateAccountsList();
        };

        $scope.updateAccountsList();
        var updateAccountsListTimer = setInterval($scope.updateAccountsList, 300000);

        $(document).ready(function () {
            var menuAPI = $("#general-menu").mmenu({
                "offCanvas": {
                    pageSelector: "#wrapper",
                    "zposition": "front"
                },
                "extensions": [
                    "pagedim-black",
                    "theme-dark",
                    "shadow-page"
                ],
                navbar: {
                    add: true,
                    title: "Menu"
                }
            }).data("mmenu");

            $("#menu-button-menu").click(function () {
                menuAPI.open();
            });

            var notificationAPI = $("#notification-menu").mmenu({
                "offCanvas": {
                    pageSelector: "#wrapper",
                    "zposition": "front",
                    "position": "right"
                },
                "extensions": [
                    "pagedim-black",
                    "theme-dark",
                    "shadow-page"
                ],
                navbar: {
                    title: 'Notifications'
                }
            }).data("mmenu");


            $("#menu-button-notifications").click(function () {
                notificationAPI.open();
            });
        });
        if ($scope.getName()) {
            bzoneAPIservice.updateOneSignalUserID();
            bzoneAPIservice.getAllPlayerNotification();
        }
        $scope.getLocalNotifications = function () {
            return bzoneAPIservice.getLocalNotifications();
        };

        $scope.getAuthNotification = function () {
            return bzoneAPIservice.getAuthNotification();
        };

        $scope.getNotificationText = function (type) {
            return bzoneAPIservice.getNotificationText(type);
        };

        $scope.markNotificationAsRead = function (notifId) {
            return bzoneAPIservice.markNotificationAsRead(notifId);
        };

        $scope.determinePastTime = function (timestamp) {
            return bzoneAPIservice.determinePastTime(timestamp);
        };
        $scope.countUnreadNotif = function () {
            return bzoneAPIservice.countUnreadNotif();
        };
    })

    .controller('authController', function ($scope, $location, bzoneAPIservice) {
        $scope.getName = function () {
            return bzoneAPIservice.getHeaderAccountName();
        };
        $scope.selectedAccount = bzoneAPIservice.getSelectedAccount();
        $scope.acceptAuth = function () {
            bzoneAPIservice.acceptAuth()
        };
        $scope.denyAuth = function () {
            bzoneAPIservice.denyAuth()
        };
        $scope.getDate = function () {
            return bzoneAPIservice.getNotificationDetailsTime();
        };
        $scope.getIp = function () {
            return bzoneAPIservice.getNotificationDetailsIp();
        };
        $scope.getLocation = function () {
            return bzoneAPIservice.getNotificationDetailsLocation();
        };
        if (bzoneAPIservice.getNotificationDetailsSeconds() > 60)
        {
            location.href = "#/expiredAuth";
        }
    })

    .controller('authConfirmationController', function ($scope, $location, bzoneAPIservice) {
        setTimeout("location.href = '#/home';", 4000);
    })

    .controller('expiredAuthController', function ($scope, $location, bzoneAPIservice) {
        setTimeout("location.href = '#/home';", 4000);
    })

    .controller('chatController', function ($scope, socket) {
        $scope.mess = null;

        console.log();
        $scope.sendMess = function () {
            var message = $scope.mess;
            console.log("Message sent: " + message);


            /*io.sockets.on("connection", function(socket) {
             socket.emit('chat message', $scope.mess);
             console.log("here");
             });

             socket.on('send:message', function (message) {
             console.log("Socket message: "+ message);
             });
             */
            socket.on('connection', function (socket) {
                socket.emit('bobo', message);

                socket.on('bobo', function (message) {
                    console.log("here");
                    $('#messages').append($('<li>').text(message));
                });

            });
            //$('#messages').append($('<li>').text(message));

        };


        /*socket.on('connection', function(socket){
         socket.on('chat message', function(msg){
         console.log(msg);
         socket.emit('chat message', msg);
         });
         });*/
    });