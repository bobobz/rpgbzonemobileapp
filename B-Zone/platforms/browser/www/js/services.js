angular.module('bzoneMobileApp.services', ['LocalStorageModule'])
    .factory('socket', function socket($rootScope) {

        var socket = io.connect('http://25.74.48.2:3000');
        console.log("reached");
        socket.on('connect', function () {
            console.log("Conn: " + socket.connected);
        });

        //var socket = io.connect('http://25.74.48.2:3000');

        return {
            on: function (eventName, callback) {
                socket.on(eventName, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
            },
            emit: function (eventName, data, callback) {
                socket.emit(eventName, data, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        if (callback) {
                            callback.apply(socket, args);
                        }
                    });
                })
            }
        };
    })

    .factory('ergastAPIservice', function ($http) {

        var ergastAPI = {};

        ergastAPI.getDrivers = function () {
            return $http({
                method: 'JSONP',
                url: 'http://ergast.com/api/f1/2013/driverStandings.json?callback=JSON_CALLBACK'
            });
        };

        ergastAPI.getDriverDetails = function (id) {
            return $http({
                method: 'JSONP',
                url: 'http://ergast.com/api/f1/2013/drivers/' + id + '/driverStandings.json?callback=JSON_CALLBACK'
            });
        };

        ergastAPI.getDriverRaces = function (id) {
            return $http({
                method: 'JSONP',
                url: 'http://ergast.com/api/f1/2013/drivers/' + id + '/results.json?callback=JSON_CALLBACK'
            });
        };

        return ergastAPI;
    })
    .service('bzoneAPIservice', function ($http, localStorageService) {
        var bzoneAPI = this;
        var accounts = [];
        var notifications = [];
        var notificationText = [];
        var numberOfUnreadNotifications = 0;

        function Account(username, sqlid, server, skinid, mobileToken) {
            this.username = username;
            this.sqlid = sqlid;
            this.server = server;
            this.skinid = skinid;
            this.mobileToken = mobileToken;

            this.status = "error";
            this.clanTag = null;
            this.formatedName = null;
            this.AuthNotification = new AuthNotification(null, null, null, null, null, null);

            this.email = null;
            this.joindate = null;
            this.formatedjoindate = null;
            this.lastlogin = null;
            this.formatedlastlogin = null;
            this.warnings = null;
            this.gold = null;
            this.ranking = null;
            this.monthlyranking = null;
            this.level = null;
            this.respect = null;
            this.hoursplayed = null;
            this.monthlyplayed = null;
            this.cash = null;
            this.bank = null;
            this.number = null;
            this.lotto = null;
            this.crimes = null;
            this.arrested = null;
            this.wanted = null;
            this.materials = null;
            this.drugs = null;
        }

        function Notification(id, type, selfref, time, isread) {
            this.id = id;
            this.type = type;
            this.selfref = selfref;
            this.time = time;
            this.isread = isread;
        }

        function AuthNotification(server, sqlid, time, md5, ip, location) {
            this.server = server;
            this.sqlid = sqlid;
            this.time = time;
            this.md5 = md5;
            this.ip = ip;
            this.location = location;
            this.isConfirmed = 1;
        }

        var selectedAccount = new Account(null, null, null, null, null);

        //localStorage.clear();
        var accountsLocalList = localStorageService.get("accountsLocalList");

        if (accountsLocalList) {
            accounts = accountsLocalList;
        }

        notificationText[1] = "Your faction application got an answer from the leader";
        notificationText[2] = "Your clan application got an answer from the clan owner";
        notificationText[3] = "New comments were added to your clan application";
        notificationText[4] = "New applications were received in your faction mailbox";
        notificationText[5] = "New applications were received in your clan mailbox";
        notificationText[6] = "One of your clan applications got more comments";
        notificationText[7] = "The server got updated";
        notificationText[8] = "The website got updated";
        notificationText[9] = "You were kicked out of your faction";
        notificationText[10] = "You were promoted by your faction leader";
        notificationText[11] = "You were demoted by your faction leader";
        notificationText[12] = "You received a warn from your faction leader";
        notificationText[13] = "You received an unwarn from your faction leader";
        notificationText[14] = "Your clan was added";
        notificationText[15] = "Your clan was renewed";
        notificationText[16] = "A clan member resigned";
        notificationText[17] = "You were invited into a clan";
        notificationText[18] = "A new member joined your clan";
        notificationText[19] = "You were kicked out of your clan";
        notificationText[20] = "You were promoted by your clan owner";
        notificationText[21] = "You were demoted by your clan owner";
        notificationText[22] = "You received a warn from your clan owner";
        notificationText[23] = "You received a unwarn from your clan owner";
        notificationText[24] = "You were reported to the admins";
        notificationText[25] = "Your complaint got more comments";
        notificationText[26] = "A complaint you are witness in got new comments";
        notificationText[27] = "Your complaint got an answer from the admins";
        notificationText[28] = "A complaint you are witness in got an answer from the admins";
        notificationText[29] = "You were reported to faction leader";
        notificationText[30] = "Your faction complaint got more comments";
        notificationText[31] = "A faction complaint you are witness in got new comments";
        notificationText[32] = "Your faction complaint got an answer from the leader";
        notificationText[33] = "A faction complaint you are witness in got an answer from the leader";
        notificationText[34] = "New faction complaints were received in your faction mailbox";
        notificationText[35] = "One of your complaints reports got more comments";
        notificationText[36] = "Leaders have been reported recently";
        notificationText[37] = "Helpers have been reported recently";
        notificationText[38] = "Admins have been reported recently";
        notificationText[39] = "Your gold amount has been changed";
        notificationText[40] = "Your complaint was withdrawn";
        notificationText[41] = "Your faction complaint was withdrawn";
        notificationText[42] = "You have been muted by an admin";
        notificationText[43] = "Your weapons license was suspended by an admin";
        notificationText[44] = "You have been jailed by an admin";
        notificationText[45] = "You have been warned by an admin";
        notificationText[46] = "You have been banned by an admin";
        notificationText[47] = "An unban request you are implicated got made";
        notificationText[48] = "Your unban request got more comments";
        notificationText[49] = "Your unban request got an asnwer";
        notificationText[50] = "A player just registered with your referral ID";
        notificationText[51] = "You have been added to a faction blacklist";
        notificationText[52] = "You have been removed from a faction blacklist";
        notificationText[53] = "You got a new blacklist warn";
        notificationText[54] = "You got a blacklist warn removed";
        notificationText[55] = "A faction complaint you are witness in was withdrawn";
        notificationText[56] = "A new interdiction request has been created";
        notificationText[57] = "You are subject to an interdiction request";
        notificationText[58] = "You received a clan invitation";
        notificationText[59] = "New inactivities were received in your faction mailbox";
        notificationText[60] = "New resignations were received in your faction mailbox";
        notificationText[61] = "Your inactivity request got an answer from the leader";
        notificationText[62] = "Your resignation request got an answer from the leader";
        notificationText[63] = "New account recovery requests have been created";
        notificationText[64] = "An account recovery request got new comments";
        notificationText[65] = "A faction you are following opened its applications";
        notificationText[66] = "Your vehicle sale got a new offer";
        notificationText[67] = "Your house sale got a new offer";
        notificationText[68] = "Your business sale got a new offer";
        notificationText[69] = "Your clan sale got a new offer";
        notificationText[70] = "Your offer to the vehicle sale got an answer";
        notificationText[71] = "Your offer to the house sale got an answer";
        notificationText[72] = "Your offer to the business sale got an answer";
        notificationText[73] = "Your offer to the clan sale got an answer";
        notificationText[74] = "You unlocked a new achievement";
        notificationText[75] = "You requested a new login on a B-Zone RPG Server";

        // Functii

        this.getAccountsList = function () {
            return accounts;
        };
        this.getSelectedAccount = function () {
            return selectedAccount;
        };

        this.getServerURL = function (server) {
            switch (server) {
                case "RPG1":
                    return 'https://rpg.b-zone.ro/mobileApi/';
                case "RPG2":
                    return 'https://rpg2.b-zone.ro/mobileApi/';
                case "RPG3":
                    return 'https://rpg3.b-zone.ro/mobileApi/';
                case "RPG4":
                    return 'https://rpg4.b-zone.ro/mobileApi/';
                case "TEST":
                    return 'https://nw.b-zone.ro/mobileApi/';
            }
        };

        this.checkLoginData = function (username, password, server) {
            var urlBuilder = bzoneAPI.getServerURL(server) + 'checkLoginData/' + username + '/' + password;
            return $http({
                method: 'GET',
                timeout: 5000,
                url: urlBuilder
            }).then(function successCallback(response) {
                if (response.data === "linkedaccount") {
                    return "linkedaccount";
                }
                else if (response.data === "false") {
                    return "false";
                }
                else {
                    selectedAccount = new Account(username, response.data.sqlid, server, response.data.skinid, response.data.mobileToken);
                    accounts.push(selectedAccount);
                    bzoneAPI.checkOnlineStatus(selectedAccount);
                    bzoneAPI.getAllPlayerNotification();
                    localStorageService.set("accountsLocalList", accounts);
                    localStorageService.set("selectedAccountLocal", selectedAccount);
                    return "true";
                }
            }, function errorCallback(response) {
                return "error";
            });
        };
        this.setHeaderAccountName = function (tag, name) {
            selectedAccount.formatedName = tag + name;
        };
        this.getHeaderAccountName = function () {
            return selectedAccount.username;
        };
        this.getHeaderServer = function () {
            return selectedAccount.server;
        };

        this.getMyAccountInfo = function () {
            var urlBuilder = bzoneAPI.getServerURL(selectedAccount.server) + 'getMyAccountInfo/' + selectedAccount.sqlid + '/' + selectedAccount.mobileToken;
            $http({
                method: 'GET',
                url: urlBuilder
            }).then(function successCallback(response) {
                if (response.data !== false) {

                    selectedAccount.email = response.data.email;
                    selectedAccount.joindate = response.data.joindate;
                    selectedAccount.formatedjoindate = moment.unix(selectedAccount.joindate).format('DD.MM.YYYY');
                    selectedAccount.lastlogin = response.data.lastlogin;
                    selectedAccount.formatedlastlogin = moment.unix(selectedAccount.lastlogin).format('DD.MM.YYYY');
                    selectedAccount.warnings = response.data.warnings;
                    selectedAccount.gold = response.data.gold;
                    selectedAccount.ranking = response.data.ranking;
                    selectedAccount.monthlyranking = response.data.monthlyranking;
                    selectedAccount.level = response.data.level;
                    selectedAccount.respect = response.data.respect;
                    selectedAccount.hoursplayed = response.data.hoursplayed;
                    selectedAccount.monthlyplayed = response.data.monthlyplayed;
                    selectedAccount.cash = response.data.cash;
                    selectedAccount.bank = response.data.bank;
                    selectedAccount.number = response.data.number;
                    selectedAccount.lotto = response.data.lotto;
                    selectedAccount.crimes = response.data.crimes;
                    selectedAccount.arrested = response.data.arrested;
                    selectedAccount.wanted = response.data.wanted;
                    selectedAccount.materials = response.data.materials;
                    selectedAccount.drugs = response.data.drugs;
                }
            }, function errorCallback(response) {
            });
        };

        this.checkOnlineStatus = function (account) {
            if (account.sqlid) {
                var urlBuilder = bzoneAPI.getServerURL(account.server) + 'getPlayerOnlineStatus/' + account.sqlid;
                return $http({
                    method: 'GET',
                    timeout: 5000,
                    url: urlBuilder
                }).then(function successCallback(response) {
                    if (response.data === 0)
                        account.status = "Offline";
                    else if (response.data === 1)
                        account.status = "Online";
                    else if (response.data === 2)
                        account.status = "AFK";
                }, function errorCallback(response) {
                    account.status = "Error";
                });
            }
        };
        this.getHeaderAccountStatus = function () {
            return selectedAccount.status;
        };

        this.updateAccountsList = function () {
            for (var i = 0; i < accounts.length; i++) {
                (function (index) {
                    var urlBuilder = bzoneAPI.getServerURL(accounts[index].server) + 'updateAccount/' + accounts[i].sqlid + '/' + accounts[i].mobileToken;

                    var tempAcc = accounts[i];
                    $http({
                        method: 'GET',
                        timeout: 5000,
                        url: urlBuilder
                    }).then(function successCallback(response) {
                        if (response.data === false) {
                            if (accounts.length > 1) {
                                accounts.splice(index, 1);
                                selectedAccount = accounts[0];
                                bzoneAPI.checkOnlineStatus(selectedAccount);
                                bzoneAPI.getAllPlayerNotification();
                                localStorageService.set("accountsLocalList", accounts);
                                localStorageService.set("selectedAccountLocal", selectedAccount);
                            }
                            else {
                                selectedAccount = new Account(null, null, null, null, null);
                                accounts.pop();
                                bzoneAPI.getAllPlayerNotification();
                                localStorageService.set("accountsLocalList", accounts);
                                localStorageService.set("selectedAccountLocal", selectedAccount);
                            }
                            return false;
                        }
                        else {
                            accounts[index].username = response.data.name;
                            accounts[index].skinid = response.data.skinid;
                            if ((accounts[index].sqlid === selectedAccount.sqlid) && (accounts[index].server === selectedAccount.server)) {
                                selectedAccount.username = response.data.name;
                            }
                            return true;
                        }
                    }, function errorCallback(response) {
                        return false;
                    });
                })(i);
            }
        };

        this.removeAccount = function (account) {
            var urlBuilder = bzoneAPI.getServerURL(account.server) + 'destroyMobileToken/' + account.sqlid + '/' + account.mobileToken;

            return $http({
                method: 'GET',
                timeout: 5000,
                url: urlBuilder
            }).then(function successCallback(response) {
                if (response.data === true) {
                    if (accounts.length > 1) {
                        for (var i = 0; i < accounts.length; i++) {
                            if (account === accounts[i])
                                accounts.splice(i, 1);
                        }
                        selectedAccount = accounts[0];
                        bzoneAPI.checkOnlineStatus(selectedAccount);
                        bzoneAPI.getAllPlayerNotification();
                        localStorageService.set("accountsLocalList", accounts);
                        localStorageService.set("selectedAccountLocal", selectedAccount);
                    }
                    else {
                        selectedAccount = new Account(null, null, null, null, null);
                        accounts.pop();
                        bzoneAPI.getAllPlayerNotification();
                        localStorageService.set("accountsLocalList", accounts);
                        localStorageService.set("selectedAccountLocal", selectedAccount);
                    }
                    return selectedAccount;
                }
            }, function errorCallback(response) {
                return false;
            });
        };

        this.switchAccount = function (account) {
            selectedAccount = account;
            bzoneAPI.checkOnlineStatus(selectedAccount);
            bzoneAPI.getAllPlayerNotification();
            localStorageService.set("accountsLocalList", accounts);
            localStorageService.set("selectedAccountLocal", selectedAccount);
        };

        this.updateOneSignalUserID = function () {
            document.addEventListener('deviceready', function () {
                // Enable to debug issues.
                //window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

                var notificationOpenedCallback = function (jsonData) {
                    //console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
                    //console.log(jsonData);
                    if (jsonData.notification.payload.additionalData.notificationType === 75) {
                        //console.log('go to auth');
                        if (selectedAccount.server !== bzoneAPI.determineServerByNumber(jsonData.notification.payload.additionalData.server) || selectedAccount.sqlid !== jsonData.notification.payload.additionalData.sqlId) {
                            var acc = bzoneAPI.determineAccount(jsonData.notification.payload.additionalData.sqlId, bzoneAPI.determineServerByNumber(jsonData.notification.payload.additionalData.server));
                            //console.log(acc);
                            if (acc) {
                                bzoneAPI.switchAccount(acc);
                            }
                        }
                        window.location.href = "#/auth";
                        //console.log('redirect');
                        var redirect = function () {
                            //console.log(window.location.hash);
                            if (window.location.hash === "#/auth") {
                                window.location.href = '#/home';
                            }
                        };

                        var redirectTimeout = setTimeout(redirect, 60000);
                    }
                    else {
                        window.location.href = "#";
                    }
                };

                var notificationReceivedCallback = function (jsonData) {
                    if (jsonData.payload.additionalData.notificationType === 75) {
                        var srv = bzoneAPI.determineServerByNumber(jsonData.payload.additionalData.server);
                        if (selectedAccount.sqlid === jsonData.payload.additionalData.sqlId && selectedAccount.server === srv) {
                            selectedAccount.AuthNotification.server = srv;
                            selectedAccount.AuthNotification.sqlid = jsonData.payload.additionalData.sqlId;
                            selectedAccount.AuthNotification.md5 = jsonData.payload.additionalData.md5;
                            selectedAccount.AuthNotification.ip = jsonData.payload.additionalData.ip;
                            selectedAccount.AuthNotification.location = jsonData.payload.additionalData.location;
                            selectedAccount.AuthNotification.time = jsonData.payload.additionalData.notificationTime;
                            selectedAccount.AuthNotification.isConfirmed = 0;

                            var removeExpired = function () {
                                selectedAccount.AuthNotification = new AuthNotification(null, null, null, null, null, null);
                                //console.log('sters');
                            };

                            var removeExpiredTimeout = setTimeout(removeExpired, 60000);
                        }
                        else {
                            var index = bzoneAPI.determineAccountIndex(jsonData.payload.additionalData.sqlId, srv);
                            //console.log(index);
                            if (index !== null) {
                                //console.log('here');
                                accounts[index].AuthNotification.server = srv;
                                accounts[index].AuthNotification.sqlid = jsonData.payload.additionalData.sqlId;
                                accounts[index].AuthNotification.md5 = jsonData.payload.additionalData.md5;
                                accounts[index].AuthNotification.ip = jsonData.payload.additionalData.ip;
                                accounts[index].AuthNotification.location = jsonData.payload.additionalData.location;
                                accounts[index].AuthNotification.time = jsonData.payload.additionalData.notificationTime;
                                accounts[index].AuthNotification.isConfirmed = 0;
                            }

                            //console.log(accounts[index]);

                            var removeExpired2 = function () {
                                accounts[index].AuthNotification = new AuthNotification(null, null, null, null, null, null);
                            };

                            var removeExpiredTimeout2 = setTimeout(removeExpired2, 60000);
                        }
                    }
                    else {
                        console.log('ia notificari');
                        bzoneAPI.getAllPlayerNotification();
                    }
                };

                window.plugins.OneSignal
                    .startInit("d55f15aa-5376-4d4c-953e-c2c394f5329c")
                    .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
                    .handleNotificationOpened(notificationOpenedCallback)
                    .handleNotificationReceived(notificationReceivedCallback)
                    .endInit();

                //console.log('autentificat');
                window.plugins.OneSignal.getIds(function (ids) {
                    for (var i = 0; i < accounts.length; i++) {
                        (function (index) {
                            var urlBuilder = bzoneAPI.getServerURL(accounts[index].server) + 'setMobileOneSignalUserId/' + accounts[index].sqlid + '/' + accounts[index].mobileToken + '/' + ids.userId;

                            $http({
                                method: 'GET',
                                timeout: 5000,
                                url: urlBuilder
                            });
                        })(i);
                    }
                });
            }, false);
        };

        this.determineServerByNumber = function (number) {
            switch (number) {
                case 0:
                    return "TEST";
                    break;
                case 1:
                    return "RPG1";
                    break;
                case 2:
                    return "RPG2";
                    break;
                case 3:
                    return "RPG3";
                    break;
                case 4:
                    return "RPG4";
                    break;
            }
            return null;
        };

        this.determineAccount = function (sqlid, server) {
            for (var i = 0; i < accounts.length; i++) {
                if (accounts[i].sqlid === sqlid && accounts[i].server === server) {
                    return accounts[i];
                }
            }
        };

        this.determineAccountIndex = function (sqlid, server) {
            for (var i = 0; i < accounts.length; i++) {
                if (accounts[i].sqlid === sqlid && accounts[i].server === server) {
                    return i;
                }
            }
        };

        this.getAllPlayerNotification = function () {
            notifications = [];
            if (selectedAccount.server) {
                var urlBuilder = bzoneAPI.getServerURL(selectedAccount.server) + 'getAllPlayerNotifications/' + selectedAccount.sqlid + '/' + selectedAccount.mobileToken;

                $http({
                    method: 'GET',
                    timeout: 5000,
                    url: urlBuilder
                }).then(function successCallback(response) {
                    if (response.data === false) {
                        return false;
                    }
                    else {
                        for (var i = 1 in response.data) {
                            var not = new Notification(response.data[i][0], response.data[i][1], response.data[i][2], response.data[i][3], response.data[i][4]);
                            notifications.push(not);
                        }
                    }
                }, function errorCallback(response) {
                    return false;
                });
            }
        };

        this.getLocalNotifications = function () {
            return notifications;
        };

        this.getAuthNotification = function () {
            return selectedAccount.AuthNotification;
        };

        this.getNotificationText = function (type) {
            return notificationText[type];
        };

        this.markNotificationAsRead = function (notifId) {
            for (var i in notifications) {
                if (notifications[i].id === notifId) {
                    notifications[i].isread = 1;
                    i = 101;
                }
            }

            var urlBuilder = bzoneAPI.getServerURL(selectedAccount.server) + 'markNotificationAsRead/' + notifId + '/' + selectedAccount.mobileToken;

            $http({
                method: 'GET',
                timeout: 5000,
                url: urlBuilder
            }).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(response) {
                return false;
            });
        };

        this.determinePastTime = function (timestamp) {
            var d = new Date(timestamp * 1000);
            var d2 = new Date();
            var d3 = new Date();
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
            d3 = Math.floor((d2 - d) / 1000);
            if (d3 < 60)
                return d3 + 's';
            else if (d3 >= 60 && d3 < 60 * 60)
                return Math.floor(d3 / 60) + 'm';
            else if (d3 >= 60 * 60 && d3 < 24 * 60 * 60)
                return Math.floor(d3 / 60 / 60) + 'h';
            else if (d3 >= 24 * 60 * 60 && d3 < 24 * 60 * 60 * 28)
                return Math.floor(d3 / 24 / 60 / 60) + 'd';
            else
                return monthNames[d.getMonth()];
        };

        this.determinePastSeconds = function (timestamp) {
            var d = new Date(timestamp * 1000);
            var d2 = new Date();
            var d3 = new Date();
            d3 = Math.floor((d2 - d) / 1000);
            return d3;
        };

        this.countUnreadNotif = function () {
            var count = 0;
            for (var i in notifications) {
                if (notifications[i].isread === 0)
                    count++;
            }
            numberOfUnreadNotifications = count;
            return count;
        };

        this.acceptAuth = function () {
            var urlBuilder = bzoneAPI.getServerURL(selectedAccount.AuthNotification.server) + 'confirmAuthNotification/' + selectedAccount.AuthNotification.sqlid + '/' + selectedAccount.mobileToken + '/' + selectedAccount.AuthNotification.md5;
            $http({
                method: 'GET',
                timeout: 5000,
                url: urlBuilder
            }).then(function successCallback(response) {
                if (response.data === "true") {
                    selectedAccount.AuthNotification = new AuthNotification(null, null, null, null, null, null);
                    return "true";
                }
                else if (response.data === "false") {
                    return "false";
                }
            }, function errorCallback(response) {
                return "error";
            });
            window.location.href = "#/authConfirmation";
        };

        this.denyAuth = function () {
            window.location.href = "#/home";
        };

        this.getNotificationDetailsTime = function () {
            return bzoneAPI.determinePastTime(selectedAccount.AuthNotification.time);
        };

        this.getNotificationDetailsIp = function () {
            return selectedAccount.AuthNotification.ip;
        };

        this.getNotificationDetailsLocation = function () {
            return selectedAccount.AuthNotification.location;
        };

        this.getNotificationDetailsSeconds = function () {
            //console.log(selectedAccount.AuthNotification.time);
            //console.log(bzoneAPI.determinePastSeconds(selectedAccount.AuthNotification.time));
            return bzoneAPI.determinePastSeconds(selectedAccount.AuthNotification.time);
        };

        var selectedAccountLocal = localStorageService.get("selectedAccountLocal");

        if (selectedAccountLocal) {
            selectedAccount = selectedAccountLocal;
            bzoneAPI.checkOnlineStatus(selectedAccount);
        }
    })
;

/*    factory('socket', ['$rootScope', function($rootScope) {
 var socket = io.connect('http://25.74.48.2:3000');


 return {
 on: function(eventName, callback){
 socket.on(eventName, callback);
 },
 emit: function(eventName, data) {
 socket.emit(eventName, data);
 }
 };
 }*/