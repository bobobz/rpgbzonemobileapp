var bzoneMobileApp = angular.module('bzoneMobileApp', [
    'bzoneMobileApp.services',
    'bzoneMobileApp.controllers',
    'ngRoute',
    'LocalStorageModule'
]);

bzoneMobileApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when("/home", {
        templateUrl: "partials/components/home.html",
        controller: "homeController"
    }).when("/drivers/:id", {
        templateUrl: "partials/driver.html",
        controller: "driverController"
    }).when("/account/general", {
        templateUrl: "partials/account/general.html",
        controller: "accountController"
    }).when("/account/accounts", {
        templateUrl: "partials/account/accounts.html",
        controller: "accountsController"
    }).when("/chat", {
        templateUrl: "partials/chat.html",
        controller: "chatController"
    }).when("/login", {
        templateUrl: "partials/account/login.html",
        controller: "loginController"
    }).when("/auth", {
        templateUrl: "partials/account/auth.html",
        controller: "authController"
    }).when("/authConfirmation", {
        templateUrl: "partials/account/authConfirmation.html",
        controller: "authConfirmationController"
    }).when("/expiredAuth", {
        templateUrl: "partials/account/expiredAuth.html",
        controller: "expiredAuthController"
    }).otherwise({redirectTo: '/home'});
}]);

bzoneMobileApp.run(function ($rootScope) {
});